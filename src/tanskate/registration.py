import csv
from bisect import bisect_left
from collections import defaultdict
from collections.abc import Sequence
from dataclasses import dataclass
from typing import Any

from .config import RegistrationConfig
from .exceptions import TanskateInputError


@dataclass
class RegistrationEntry:
    start_no: int
    meta: list[str]
    categories: set[str]


class RegistrationResults:
    def __init__(self, entries: list[RegistrationEntry]):
        self.entries = entries
        self.entries.sort(key=lambda entry: entry.start_no)

    def category_participants_count(self) -> dict[str, int]:
        result = defaultdict(int)
        for entry in self.entries:
            for category_key in entry.categories:
                result[category_key] += 1
        return dict(result)

    def category_entries(self, category_key) -> list[RegistrationEntry]:
        return [entry for entry in self.entries if category_key in entry.categories]

    def find_entries(self, start_nos: Sequence[int]) -> list[RegistrationEntry]:
        result = []

        for start_no in start_nos:
            i = bisect_left(self.entries, start_no, key=lambda entry: entry.start_no)
            if i >= len(self.entries) or self.entries[i].start_no != start_no:
                raise KeyError(start_no)
            result.append(self.entries[i])

        return result

    @staticmethod
    def load(config: RegistrationConfig) -> "RegistrationResults":
        entries = []

        with open(config.results_path, newline="") as f:
            reader = csv.DictReader(f)
            for line in reader:
                try:
                    start_no = line[config.start_no_column]
                    meta = [line[key] for key in config.meta_columns]
                    categories = line[config.categories_column]
                except KeyError as e:
                    raise TanskateInputError(
                        f"Column '{e.args[0]}' "
                        "is defined in config, but does not exist in "
                        f"'{config.results_path}'"
                    )

                if not start_no:
                    continue

                # TODO: validation
                categories = {cat.strip() for cat in categories.split(",")}
                entry = RegistrationEntry(int(start_no), meta, categories)
                entries.append(entry)

        return RegistrationResults(entries)


def build_meta_table(
    entries: Sequence[RegistrationEntry], meta_columns_header: list[str]
) -> list[list[Any]]:
    header_row = ["#"] + meta_columns_header
    rows = [[entry.start_no, *entry.meta] for entry in entries]
    return [header_row, *rows]
