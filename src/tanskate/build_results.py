import sys
from pathlib import Path
from typing import Annotated, Optional

from typer import Option

from .config import CompetitionConfig
from .registration import RegistrationResults, build_meta_table


def build_results(
    config: Annotated[Path, Option()],
    category_key: Annotated[Optional[str], Option()] = None,
):
    """
    Create a table with category results

    Stdin: TSV data from the protocol sheet of the final round. The last column is the
    final rank

    Stdout: TSV table with participants' metadata and ranks
    """

    comp_config = CompetitionConfig.load(config)
    reg_results = RegistrationResults.load(comp_config.registration)

    start_nos = []
    ranks = []

    for line in sys.stdin:
        values = line.strip().split("\t")
        start_nos.append(int(values[0]))
        ranks.append(values[-1])

    entries = reg_results.find_entries(start_nos)
    meta_table = build_meta_table(entries, comp_config.registration.meta_columns)

    if category_key is not None:
        print(comp_config.categories[category_key].name, end="\n\n")

    print("Место", *meta_table[0], sep="\t")

    for rank, row in zip(ranks, meta_table[1:]):
        print(rank, *row, sep="\t")
