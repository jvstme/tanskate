from openpyxl.styles import Alignment, Border, Font, NamedStyle, Side

# Common

default_font = "Arial"
default_normal = Font(
    name=default_font,
)
default_bold = Font(
    name=default_font,
    bold=True,
)
thin = Side(style="thin", color="000000")
thick = Side(style="thick", color="000000")
thin_border = Border(
    top=thin,
    right=thin,
    bottom=thin,
    left=thin,
)
thick_border = Border(
    top=thick,
    right=thick,
    bottom=thick,
    left=thick,
)
centered = Alignment(
    horizontal="center",
    vertical="center",
)


# Heats

heats_title = NamedStyle(
    name="heats-title",
    font=Font(
        name=default_font,
        bold=True,
        size=24,
    ),
)

heats_column_title = NamedStyle(
    name="heats-column-title",
    font=Font(
        name=default_font,
        bold=True,
        size=14,
    ),
    border=thin_border,
    alignment=centered,
)

heats_participant_no = NamedStyle(
    name="heats-participant-no",
    font=Font(
        name=default_font,
        bold=True,
        size=18,
    ),
    border=thin_border,
    alignment=centered,
)

heats_heat = NamedStyle(
    name="heats-heat",
    font=Font(
        name=default_font,
        size=18,
    ),
    border=thin_border,
    alignment=centered,
)

heats_title_row_height = 30
heats_column_title_row_height = 18
heats_table_row_height = 43


# Blank

blank_header_key = NamedStyle(
    name="blank-header-key",
    font=default_normal,
    alignment=Alignment(
        horizontal="right",
    ),
)

blank_header_value = NamedStyle(
    name="blank-header-value",
    font=default_bold,
    alignment=Alignment(
        horizontal="left",
    ),
)

blank_heat_name = NamedStyle(
    name="blank-heat-name",
    font=Font(
        name=default_font,
        bold=True,
        size=14,
    ),
    border=thick_border,
    alignment=Alignment(
        horizontal="center",
        vertical="center",
        text_rotation=90,
    ),
)

blank_table_top_header = NamedStyle(
    name="blank-table-top-header",
    font=default_bold,
    alignment=centered,
    border=Border(
        top=thick,
        right=thin,
        bottom=thick,
        left=thin,
    ),
)

blank_table_participant_cell = NamedStyle(
    name="blank-table-participant-cell",
    font=default_bold,
    alignment=centered,
)

blank_heat_name_col_width = 3.75
blank_table_col_width = 11
blank_table_row_height = 18
blank_header_row_height = 16


# Protocol

protocol_table_header = NamedStyle(
    name="protocol-table-header",
    font=default_bold,
    alignment=centered,
    border=thin_border,
)

protocol_table_data = NamedStyle(
    name="protocol-table-data",
    font=default_normal,
    alignment=centered,
)

protocol_info_col_width = 7
protocol_mark_col_width = 2.5
protocol_judge_no_col_width = 3
