import pytest

from final_examples import (
    examples,
    to_dance_results_list,
    to_full_marks_list,
    to_full_results_list,
    to_participant_list,
)
from tanskate.count_final import (
    ParticipantMarks,
    calculate_dance_results,
    calculate_final_ranks,
    calculate_full_ranks,
    gen_table,
    infer_dances_judges_count,
    parse_table,
)
from tanskate.exceptions import TanskateInputError


def test_participant_marks():
    participant = ParticipantMarks([4, 3, 5, 3, 2], 6)
    assert participant.marks == [4, 3, 5, 3, 2]
    assert participant.participants_count == 6
    assert participant.higher_marks_count == [0, 1, 3, 4, 5, 5]
    assert participant.higher_marks_sum == [0, 2, 8, 12, 17, 17]


def test_participant_marks_with_fractions():
    participant = ParticipantMarks([1, 2, 2.5, 3], 6)
    assert participant.marks == [1, 2, 2.5, 3]
    assert participant.participants_count == 6
    assert participant.higher_marks_count == [1, 2, 4, 4, 4, 4]
    assert participant.higher_marks_sum == [1, 3, 8.5, 8.5, 8.5, 8.5]


def get_sorted_participants(example):
    ranks = to_participant_list(example)
    calculated_ranks = calculate_dance_results([r.participant for r in ranks])
    ranks.sort(key=lambda r: r.participant.start_no)
    calculated_ranks.sort(key=lambda r: r.participant.start_no)
    return calculated_ranks, ranks


@pytest.mark.parametrize(
    "example_key", ["А", "Б", "В", "Г", "Д", "Е", "Ж", "З", "И", "К"]
)
def test_sorted_participants(example_key):
    sorted_participants, expected_sorted_participants = get_sorted_participants(
        examples[example_key]
    )
    assert sorted_participants == expected_sorted_participants


def get_final_ranks(example):
    dance_results_list = to_dance_results_list(example)
    expected_ranks = dict(zip(example["start_nos"], example["results"]))
    ranks = calculate_final_ranks(dance_results_list)
    return ranks, expected_ranks


@pytest.mark.parametrize("example_key", ["Л", "М", "Н"])
def test_final_ranks(example_key):
    ranks, expected_ranks = get_final_ranks(examples[example_key])
    assert ranks == expected_ranks


def test_final_full():
    example = examples["О"]
    dance_results = to_full_results_list(example)
    ranks = calculate_final_ranks(dance_results)
    expected_ranks = dict(zip(example["start_nos"], example["results"]))
    assert ranks == expected_ranks


def get_full_ranks_from_marks(example):
    dance_marks = to_full_marks_list(example)
    expected_ranks = dict()
    for i in range(len(example["start_nos"])):
        expected_ranks[example["start_nos"][i]] = (
            [dance[i] for dance in example["dance_results"]],
            example["results"][i],
        )
    ranks = calculate_full_ranks(dance_marks)
    return ranks, expected_ranks


def test_full():
    ranks, expected_ranks = get_full_ranks_from_marks(examples["О"])
    assert ranks == expected_ranks


def test_full_shared():
    ranks, expected_ranks = get_full_ranks_from_marks(examples["П"])
    assert ranks == expected_ranks


# TODO:
# def test_parse_table(): pass
# def test_gen_table(): pass


def test_infer_dances_judges_count():
    table = [
        ["10", "Alice", "Bob", "1", "2", "3", "4", "5", "6"],
    ]
    meta_cols_count = 2

    assert infer_dances_judges_count(table, meta_cols_count, 3, 2) == (3, 2)
    assert infer_dances_judges_count(table, meta_cols_count, None, 2) == (3, 2)
    assert infer_dances_judges_count(table, meta_cols_count, 3, None) == (3, 2)

    with pytest.raises(TanskateInputError):
        infer_dances_judges_count(table, meta_cols_count, None, None)


def test_infer_dances_judges_count_broken_table():
    table = [
        ["10", "Alice", "1", "2", "3", "4", "5"],
    ]
    meta_cols_count = 1

    with pytest.raises(TanskateInputError):
        infer_dances_judges_count(table, meta_cols_count, None, 2)

    with pytest.raises(TanskateInputError):
        infer_dances_judges_count(table, meta_cols_count, 3, None)


def test_parse_calculate_gen_table():
    table = [
        ["11", "Алёна", "Борис", "", ""]
        + ["1", "1", "2", "2", "2", "", ""]
        + ["2", "2", "1", "2", "2", "", ""]
        + ["2", "2", "2", "1", "2", "", ""],
        ["12", "Валентина", "Геннадий", "", ""]
        + ["2", "2", "1", "1", "1", "", ""]
        + ["1", "1", "2", "1", "1", "", ""]
        + ["1", "1", "1", "2", "1", "", ""],
    ]

    dance_marks, unused_judges_count = parse_table(
        table,
        dances_count=3,
        used_judges_count=5,
        meta_cols_count=4,
    )
    full_ranks = calculate_full_ranks(dance_marks)
    results_table = gen_table(dance_marks, unused_judges_count, full_ranks)

    expected_table = [
        ["12", "Валентина", "Геннадий", "", ""]
        + ["2", "2", "1", "1", "1", "", ""]
        + ["1", "1", "2", "1", "1", "", ""]
        + ["1", "1", "1", "2", "1", "", ""]
        + ["1", "1", "1", "1"],
        ["11", "Алёна", "Борис", "", ""]
        + ["1", "1", "2", "2", "2", "", ""]
        + ["2", "2", "1", "2", "2", "", ""]
        + ["2", "2", "2", "1", "2", "", ""]
        + ["2", "2", "2", "2"],
    ]

    assert results_table == expected_table
