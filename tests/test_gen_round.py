import os
from itertools import chain
from pathlib import Path

import pytest

from tanskate.config import CategoryConfig
from tanskate.exceptions import TanskateInputError
from tanskate.gen_round import (
    Round,
    choose_workbook_path,
    map_participant_to_heat,
    split_columns,
    split_heats,
)


def test_split_heats_18_by_3():
    heats = split_heats(range(18), 3)
    assert len(heats) == 3
    assert set(heats[0] + heats[1] + heats[2]) == set(range(18))
    for heat in heats:
        assert len(heat) == 6
        assert heat == sorted(heat)


def test_split_heats_11_by_4():
    heats = split_heats(range(11), 4)
    assert len(heats) == 4
    assert set(heats[0] + heats[1] + heats[2] + heats[3]) == set(range(11))
    assert len(heats[0]) == len(heats[1]) == len(heats[2]) == 3
    assert len(heats[3]) == 2
    for heat in heats:
        assert heat == sorted(heat)


def test_split_heats_2_by_4():
    heats = split_heats(range(2), 4)
    assert len(heats) == 4
    assert len(heats[0]) == len(heats[1]) == 1
    assert len(heats[2]) == len(heats[3]) == 0
    assert set(heats[0] + heats[1]) == set(range(2))


def test_split_heats_10_by_1():
    heats = split_heats(range(9, -1, -1), 1)
    assert len(heats) == 1
    assert heats[0] == list(range(10))


def test_split_heats_10_by_0():
    with pytest.raises(ValueError):
        split_heats(range(10), 0)


def test_split_heats_10_by_neg_1():
    with pytest.raises(ValueError):
        split_heats(range(10), -1)


def test_split_heats_negative_participant_nos():
    heats = split_heats([0, -2, 5, 3], 2)
    assert len(heats) == 2
    assert len(heats[0]) == len(heats[1]) == 2
    assert set(heats[0] + heats[1]) == {0, -2, 5, 3}
    for heat in heats:
        assert heat == sorted(heat)


def test_split_heats_non_unique_participant_nos():
    heats = split_heats([1] * 6, 2)
    assert len(heats) == 2
    assert len(heats[0]) == len(heats[1]) == 3
    assert set(heats[0] + heats[1]) == {1}


def test_map_participant_to_heat():
    heats = [
        [1, 3, 6, 7],
        [2, 4, 5],
    ]
    expected = [(1, 1), (2, 2), (3, 1), (4, 2), (5, 2), (6, 1), (7, 1)]
    assert map_participant_to_heat(heats) == expected


def test_map_participant_to_heat_negative_participant_nos():
    heats = [
        [25, -43, 39],
        [-12, -47, 23],
    ]
    expected = [(-47, 2), (-43, 1), (-12, 2), (23, 2), (25, 1), (39, 1)]
    assert map_participant_to_heat(heats) == expected


def test_map_participant_to_heat_non_unique_participant_nos():
    heats = [
        [0, 1, 0],
        [1, 0, 1],
        [0, 1, 0],
    ]
    expected = [(0, 1), (0, 1), (0, 2), (0, 3), (0, 3), (1, 1), (1, 2), (1, 2), (1, 3)]
    assert map_participant_to_heat(heats) == expected


@pytest.mark.parametrize(
    (
        "participants_count",
        "expected_column_lengths",
    ),
    [
        (3, [3]),
        (4, [2, 2]),
        (5, [3, 2]),
        (8, [4, 4]),
        (9, [3, 3, 3]),
    ],
)
def test_split_columns(participants_count, expected_column_lengths):
    column_thresholds = [3, 8]
    participant_to_heat = [(i, i) for i in range(participants_count)]
    columns = split_columns(participant_to_heat, column_thresholds)

    assert list(chain(*columns)) == participant_to_heat
    assert list(map(len, columns)) == expected_column_lengths


def test_choose_workbook_path(tmp_path: Path):
    os.chdir(tmp_path)
    nested_path = tmp_path / "nested"
    nested_path.mkdir()

    round = Round(
        CategoryConfig(name="Open Latin", dances=["S", "C", "R", "P", "J"], judges=[]),
        "Final",
        0,
        [],
    )
    assert (
        choose_workbook_path(None, None, round).absolute()
        == tmp_path.absolute() / "Open Latin, Final.xlsx"
    )
    assert (
        choose_workbook_path(None, nested_path, round).absolute()
        == nested_path.absolute() / "Open Latin, Final.xlsx"
    )
    assert (
        choose_workbook_path(nested_path / "round.xlsx", None, round).absolute()
        == nested_path.absolute() / "round.xlsx"
    )
    with pytest.raises(TanskateInputError):
        choose_workbook_path(nested_path / "round.xlsx", nested_path, round)
