from pathlib import Path

from tanskate.config import CompetitionConfig


def test_correct():
    data = dict(
        registration=dict(
            results_path=Path("/dev/null"),
            start_no_column="#",
            categories_column="categories",
            meta_columns=["meta 1", "meta 2"],
        ),
        categories=dict(
            first=dict(
                name="First",
                dances=["d1", "d2", "d3"],
                judges=["j1", "j2", "j3"],
            ),
        ),
    )

    config = CompetitionConfig(**data)
    assert config.model_dump() == data
